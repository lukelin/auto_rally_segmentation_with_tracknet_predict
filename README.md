###### tags: `CoachAI` `TrackNetV2` `GitLab` `Rally cut`

# Auto Rally cut with TrackNet prediction (GitLab)

## :gear: 1. Install
### System Environment
        
        $ sudo apt-get install git
        $ sudo apt-get install python3-pip
        $ pip3 install opencv-python
        $ pip3 install numpy
        $ git clone https://gitlab.com/lukelin/auto_rally_segmentation_with_tracknet_predict
        
## :clapper: 2. Auto Rally Segmentation with TrackNet Result

### Step 1 : Get TrackNet Result ###

TrackNetV2 : https://gitlab.com/lukelin/pytorch-tracknetv2-6-in-6-

### Step 2 : Rally cut 

`python3 rally_cut.py --input_video=<videoPath> --input_csv=<csvPath>`
    
Just put the video path you want to predict on option `<videoPath>` , and the predict csv file of TrackNetV2 on `<csvPath>`.

#### The predict csv after apply TrackNetV2 should look like : ####

![](https://i.imgur.com/6FcPJE4.png)
### . ###
### . ###
### . ###
![](https://i.imgur.com/5jKnqUB.png)


#### After command your rally videos will under `rally_video` folder ####

Output rally videos : https://youtu.be/taHJ2IMgC0E
and you will get the csv file with rally column (empty means current frame is in rest).

#### The predict csv after apply `rally_cut.py` should look like : ####

![](https://i.imgur.com/U5RHuw4.png)
### . ###
### . ###
### . ###
![](https://i.imgur.com/Rm9Hzdq.png)


