import os
import csv
import cv2
import math
import argparse
import numpy as np


parser = argparse.ArgumentParser(description = 'Auto rally cut with TrackNet predict result')
parser.add_argument('--input_video', type = str, help = 'input video name')
parser.add_argument('--input_csv', type = str, help = 'input csv name')
args = parser.parse_args()

filename = args.input_csv
input_video_path = args.input_video


list1 = []
vis=[]
frames=[]
x=[]
y=[]
time=[]

with open(filename, newline='') as csvFile:
    rows = csv.reader(csvFile, delimiter=',')
    for row in rows:
        list1.append(row)
    for i in range(1,len(list1)):
        frames.append(int(float(list1[i][0])))
        vis.append(int(float(list1[i][1])))
        x.append(int(float(list1[i][2])))
        y.append(int(float(list1[i][3])))
        time.append(list1[i][4])
print(len(vis))


start_list=[]
end_list = []
num = 0
check_start = 0
check_end = 0
val_start = 0
val_end = 0
i = 0

while i < len(frames):

	if(vis[i] == 1 and check_start == 0):
		if((i+90) < len(frames)):
			for j in range(i, i+90):
				if(vis[j] == 0):
					val_start += 1
				
			if(val_start < 30):
				start_list.append(i-90)
				check_start = 1
				check_end = 0
				i+=90
		val_start = 0

	if(vis[i] == 0 and check_end == 0):
		if((i+90) < len(frames)):
			for j in range(i, i+90):
				if(vis[j] == 1):
					val_end += 1
				
			if(val_end < 15):
				end_list.append(i+90)
				check_start = 0
				check_end = 1
				i+=90
		val_end = 0
	i += 1

print(start_list)
print(len(start_list))
print(end_list)
print(len(end_list))

###############################video output####################################
if not os.path.exists('rally_video'):
	os.makedirs('rally_video')

currentFrame = 0
rally = 1

video = cv2.VideoCapture(input_video_path)
fps = int(video.get(cv2.CAP_PROP_FPS))
total_frame=video.get(cv2.CAP_PROP_FRAME_COUNT)
rally_array = np.zeros(int(total_frame))
output_width = 1280
output_height = 720
fourcc = cv2.VideoWriter_fourcc(*'XVID')

for i in range(len(start_list)):
	output_video_path = 'rally_video/'+ str(rally) +'.mp4'
	output_video = cv2.VideoWriter(output_video_path,fourcc, fps, (output_width,output_height))
	video.set(cv2.CAP_PROP_POS_FRAMES, start_list[i])
	currentFrame = start_list[i]
	while(True):
		ret, img = video.read()
		if not ret: 
			break
		if currentFrame == end_list[i+1]:
			break
		rally_array[currentFrame] = rally
		img = cv2.resize(img,(1280,720))
		output_video.write(img)
		currentFrame += 1
	#if rally == 10:
	#	break

	print("Output rally ",rally)
	output_video.release()
	rally += 1

with open(filename[:-4] + '_rally.csv','w', newline='') as csvfile1:
	r = csv.writer(csvfile1)
	r.writerow(['Frame','Visibility', 'X','Y','Time','Rally'])
	for i in range(len(frames)):
		if(rally_array[i] == 0):
			r_idx = ""
		else:
			r_idx = str(rally_array[i])
		r.writerow([frames[i] , vis[i] , x[i] , y[i] , time[i] , r_idx])


video.release()
print("finish")
